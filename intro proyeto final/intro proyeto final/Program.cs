﻿using System;

namespace intro_proyeto_final
{
    class Program
    {
        static void Main(string[] args)
        {/*En el siguiente proyecto crearemos una aplicación que pida al usuario su (username y clave). El usuario sera el No. de cedula y la clave debe ser numerica,
            el sistma no debe permitir que el usuario acceda hasta que ponga su autenticacion correcta.

            El sistema debe tener al menos 3 usuarios registrados para validar, y los usuarios al acceder tendran un rol (Supervisor, 
            Administrador, Vendedor). De acuerdo a su rol le mostrara el mensaje al acceder "Acabas de ingresar con el usuario XXXX y su rol es XXXXX.

           Los usuarios algunos tendran un estado de (activo o inactivo) en caso de inactivo sea positivo el usuario no podra acceder,  
           el sistema debe mostrar un mensaje que ese usuario esta inactivo y volver a validar otro usuario.

           Datos de usuarios: Nombre, username, clave, rol, fecha de creacion, estado (Activo o inactivo).*/


            string usur1;
            //usur1 = 123-1234567-5 , usur2 = 123-1234568-6 , usur3 = 123-1234569-7
            int clave1;
            DateTime creacion1 = new DateTime(2015, 9, 7, 5, 30, 23);
            DateTime creacion2 = new DateTime(2013, 5, 6, 9, 15, 16);

            string salir;

                do
                {
                   Console.Clear();

                    Console.WriteLine("precione enter para empezar");


                    salir = Console.ReadLine();

                    Console.Write("Digite su usuario: ");
                    usur1 = Console.ReadLine();

                    if (usur1 == "123-1234567-5")
                    {
                        do
                        {
                            Console.Write("Digite su clave: ");
                            clave1 = int.Parse(Console.ReadLine());

                            Console.WriteLine("Clave incorreta, intentelo otra vez");
                        } while (clave1 != 1234);
                        Console.Clear();

                        Console.WriteLine(" Bienvenido, Alejandro Bruno \n \n Acabas de ingresar con el usuario: " + usur1 + " \n tu rol es: Supervisor \n  Fecha de creacion: " + creacion1.ToString("MM/dd/yyyy hh:mm:ss"));

                        Console.WriteLine("\nPulse s para salir");
                      salir = Console.ReadLine();
                    }
                    else if (usur1 == "123-1234568-6")
                    {
                        do
                        {
                            Console.Write("Digite su clave: ");
                            clave1 = int.Parse(Console.ReadLine());
                            Console.WriteLine("Clave incorreta, intentelo otra vez");
                        } while (clave1 != 1235);
                        Console.Clear();

                        Console.WriteLine(" Bienvenido, Antonio Perez \n \n Acabas de ingresar con el usuario: " + usur1 + " \n tu rol es: Administrador \n Fecha de creacion: "  + creacion2.ToString("MM/dd/yyyy hh:mm:ss"));
                        Console.WriteLine("\nPulse s para salir");
                      salir = Console.ReadLine();
                    }
                    else if (usur1 == "123-1234569-7")
                    {
                        do
                        {
                            Console.Write("Digite su clave: \n");
                            clave1 = int.Parse(Console.ReadLine());

                        } while (clave1 != 1236);
                        Console.Clear();

                        Console.WriteLine("Este usuario esta inactivo \n favor ingrese con otro. pulse s para salir ");

                        salir = Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("El usuario es incorrecto, pulse s empezar de nuevo");
                        salir = Console.ReadLine();
                    }

                } while (salir == "s" || salir == "S");

            Console.WriteLine("Ha salido del programa.");

                Console.ReadKey(); 
        }
    }
}
